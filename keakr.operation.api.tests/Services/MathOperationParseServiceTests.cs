using NUnit.Framework;
using keakr.operation.api.Services;

namespace keakr.operation.api.tests.Services
{
    public class MathOperationParseServiceTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void ParseNull_shouldBe_Zero()
        {
            var result = MathOperationParseService.Parse(null);

            Assert.AreEqual(0, result);
        }

        [Test]
        public void ParseEmptyString_shouldBe_Zero()
        {
            var result = MathOperationParseService.Parse("");

            Assert.AreEqual(0, result);
        }

        [Test]
        public void ParseInvalidExpression_shouldBe_Zero()
        {
            var result = MathOperationParseService.Parse("invalid string");

            Assert.AreEqual(double.NaN, result);
        }

        [Test]
        public void Parse_5Div0_shouldBe_Nan()
        {
            var result = MathOperationParseService.Parse("5/0");

            Assert.AreEqual(double.NaN, result);
        }

        [Test]
        public void Parse_5plus3_shouldBe_8()
        {
            var result = MathOperationParseService.Parse("5+3");

            Assert.AreEqual(8, result);
        }

        [Test]
        public void Parse_5x3_shouldBe_15()
        {
            var result = MathOperationParseService.Parse("5x3");

            Assert.AreEqual(15, result);
        }

        [Test]
        public void Parse_ExpressionWithBrackets_shouldBe_20()
        {
            var result = MathOperationParseService.Parse("5+5x(2+1)");

            Assert.AreEqual(20, result);
        }
    }
}