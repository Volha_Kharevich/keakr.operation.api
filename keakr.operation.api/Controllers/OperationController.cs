﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using keakr.operation.api.Models;
using keakr.operation.api.Services;

namespace keakr.operation.api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OperationController : ControllerBase
    {
        // POST api/operation
        [HttpPost]
        public OperationResponse Post([FromBody]  OperationRequest request)
        {
            return new OperationResponse() { Result = MathOperationParseService.Parse(request.Operation) };
        }
    }
}
