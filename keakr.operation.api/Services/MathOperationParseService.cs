
using System;
using System.Text.RegularExpressions;
using org.mariuszgromada.math.mxparser;


namespace keakr.operation.api.Services
{
    public static class MathOperationParseService
    {
        public static double Parse(string operationString)
        {
            if(String.IsNullOrEmpty(operationString))
            {
                return 0;
            }
            Expression exp = new Expression(new Regex("[x]").Replace(operationString, "*"));
            return exp.calculate();
        }
    }
}